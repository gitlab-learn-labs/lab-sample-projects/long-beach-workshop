# Step 1: Defining Our Framework
  1. First, in a new tab, navigate [here](https://gitlab.com/gitlab-learn-labs/sample-projects/long-beach-compliance-framework) to see the compliance framework we intend on applying to our project.
  2. Open **_.compliance-gitlab-ci.yml_** and take a look at the stages section, where each stage defined must be run in order for each pipeline the framework is applied to. Note that the ***.pre*** stage starts with "." so that it doesn't have to be defined to be run.
  3. Below the stages, we define a simple ***compliance_job*** that will run during the ***.pre*** stage that just prints out a message letting the user know the framework has been applied. 
  4. We also include required jobs to be included in each project we assign our Compliance Framework to.  The jobs within a compliance pipeline are not able to be overridden by the project team.
  5. Lastly, at the end of .compliance-gitlab-ci.yml, the "include" section allows pipelines that the framework is applied on, to build off of the job definitions. 

> [Docs for Compliance Pipeline](https://docs.gitlab.com/ee/user/group/compliance_frameworks.html#compliance-pipelines)

# Step 2: Applying the Framework
  1. You can now close this tab and navigate back to your **Long Beach Workshop** project. Once there use the left hand navigation menu to click **Settings > General**. Next scroll down to the ***Compliance framework*** and click the **Choose your Framework** dropdown.
  2. On the resulting drop down select **Long Beach CF** then click **Save changes**.
  3. Now if you go back to your project home screen by clicking the name of your project in the top left you can see that the framework has been applied. Note that our compliance template is defined at the top of the group allowing us to apply it on any projects in the group.
