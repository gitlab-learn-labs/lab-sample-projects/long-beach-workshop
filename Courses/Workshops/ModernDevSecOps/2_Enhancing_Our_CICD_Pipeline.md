# Step 01 - Pipeline Speed With Caching

1. We saw after committing our last change that the pipeline does take some time to run. Our next few pipelines will build off of our current configuration, so why dont we add some caching for the node_modules so that they dont have to be pulled next time?
2. Let's open our **Pipeline Editor**, by selecting it from the **Edit** dropdown. Add the code below under our **_image_** definition so that our node modules will be cached:

   ```plaintext
    # Cache modules in between jobs
    cache:
      - key: cache-$CI_COMMIT_REF_SLUG
        fallback_keys:
          - cache-$CI_DEFAULT_BRANCH
          - cache-default
        paths:
          - vendor/ruby
          - Gemfile.lock
   ```
3. Now that this code has been added, the shared runners will cache this information for us to pull next time we run the pipeline. Before committing we will complete the next step.

# Step 02 - Adding Rules to Specify Behavior

1. What if our **_super_fast_test_** job starts failing? Lets modify **- return 0** to **- exit 1** in the **_script_** to make it fail:

   ```plaintext
   - exit 1
   ```
2. What if we also want to allow failure on a rule that we had set? Let's test that out on the **_test_** job. **Change** the rules to be the below code:

   ```plaintext
   rules:
       - if: $CI_COMMIT_BRANCH == 'main'
         allow_failure: true
   ```
3. Your yaml for the **_test_** & **_super_fast_test_** job should look like this:

   ```plaintext
    test:
      stage: test
      image: gliderlabs/herokuish:latest
      script:
        - cp -R . /tmp/app
        - /bin/herokuish buildpack test
      after_script:
        - echo "Our race track has been tested!"
      needs: []
      

    super_fast_test:
      stage: test
      script:
        - echo "If your not first you're last"
        - exit 1
      needs: []
      rules:
        - if: $CI_COMMIT_BRANCH == 'main'
        allow_failure: true
     
        
   ```

# Step 03 - Securing the Pipeline

2. Next, we want to ensure that our pipeline stays secure. To do this, we will add a few of GitLab's security scanners, starting with SAST. Under where we define the image for our pipeline to use, add the code below to include the SAST template:

   ```plaintext
   include:
     - template: Security/SAST.gitlab-ci.yml
   ```
3. To take a look at the template we just added, look near the top of the edit page next to where you can select the branch (branch will show **_main_** right now) there will be a **_tree expand icon_** we want to click. From here we can see a link to all of the templates we currently have included in our pipeline.
4. We then want to click the link for **Jobs/SAST.gitlab-ci.yml** that will open a new window where you can view the contents of the job we have included. Spend some time looking this over then close out the window to get back to our pipeline editor screen.
5. Now that we have SAST lets add a few more security templates to our project to confirm that our code is secure. Edit your **_include_** section below to be:

   ```plaintext
   include:
     - template: Security/SAST.gitlab-ci.yml
     - template: Code-Quality.gitlab-ci.yml
     - template: Jobs/Dependency-Scanning.gitlab-ci.yml
     - template: Jobs/SAST.gitlab-ci.yml
     - template: Jobs/Secret-Detection.gitlab-ci.yml
   ```
6. We will also now want to edit our stages to ensure we can run all of these templates:

   ```plaintext
   stages:
     - build
     - test
     - security
   ```
7. Also if you click the tree icon or **_Full configuration_** tab you should be able to see the contents of all the new security templates we have just added. Before committing we will add some inheritance in the next step.

# Step 04 - Using Inheritance to Enhance Our Pipeline

1. Now we will create a new **_sast_** job that will overwrite some of the functionality from the template we included. The below code sets up the new job and has it run as soon as the pipeline begins:

   ```plaintext
    # overwrite SAST job stage and start job as soon as pipeline begins
   sast:
     stage: security
     needs: []
   ```
2. Before commiting the code, let's add the **_artifact_** keyword in the next step.

# Step 05 - Storing with Artifacts

1. Let's say a requirement comes in that we want to store the results of the **_build_** job in an artifact. Let's add a change the job to do just that:

   ```plaintext
   build:
    stage: build
    variables:
      IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
    before_script:
      - docker info
      - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    script:
      - docker build -t $IMAGE .
    after_script:
      - docker push $IMAGE
    artifacts:
      paths:
        - Gemfile.lock
      expire_in: 1 hour


   ```
2. Now when we go ahead and click **Commit Changes** and use the left hand menu to click through **Build \> Pipelines**, then click the hyperlink from the most recently kicked off pipeline that starts with **<span dir="">_#_</span>**. We can see the changes that took place by using inheritance on the **_sast_** job & we will wait for the pipeline to complete before moving on to the next step. If you got lost at any point you can just merge in the **_extended-sast_** branch as well.

   > Note that we have also added a new stage which continues to run even though our **_super_fast_test_** job is failing.
3. We are then going to grab the build artifact we specified in the pipeline. There are a few different ways to get them, but we are just going to use the left hand menu to click through **Build \> Pipelines** where in the most recent pipeline that kicked off you should see a download icon. Click that icon then click **build_app:archive**.
4. Go ahead and inspect the artifact you just downloaded, this will be the results of the **_build_app_** artifact we added. You can also download and inspect the sast scan results.

# Step 06 - Preparing the Child Pipeline

1. Your security team got wind of all of the security scanning you added to your pipeline and wants it to be added to a new pipeline that they have access to and can control which jobs are ran. To do this, we are going to create a child pipeline that we can link from our existing pipeline. In this case we will just be adding the second pipeline to this project, but it could have been added in a separate project and then linked.
2. To create this pipeline we will first want to click the name of our project in the top left then click **Web IDE** from the main project page. Once in the IDE at the top level of the project we will want to create a new folder and call it **security-pipeline**.
3. The folder **_security-pipeline_** directory should be open after you create the directory. Lets go ahead and create a new file  named **security.gitlab-ci.yml**. The new file will now be open.
4. Copy and paste the code into the new pipeline config below. Notice that most of it is pulled from the existing pipeline we have been working on throughout the workshop:

   ```plaintext
   image: docker:latest
   
   include:
     - template: Code-Quality.gitlab-ci.yml
     - template: Jobs/Dependency-Scanning.gitlab-ci.yml
     - template: Jobs/SAST.gitlab-ci.yml
     - template: Jobs/Secret-Detection.gitlab-ci.yml
   ```
5. This pipeline now has all it needs to run our security tests.
6. Once added, click the source control button on the left hand side, add a quick commit message, then click **Commit & Push**. On the resulting drop down click no to open a new branch and just commit to **_main_**. Once done a popup will appear and we want to click **Go to project**.
7. Once back in the project use the left hand navigation menu to click through **Build \> Pipeline editor** so we can now set up the connection to our security pipeline.

# Step 07 - Linking the Pipelines

1. First we are going to want to remove all of the duplicated code in our existing pipeline. At the same time, let's resolve our forced failure in **super_fast_test** by changing **-exit 1** to **-return 0**. The code below will be our existing pipeline minus everything the security pipeline now covers:

   ```plaintext
    image: docker:latest

    cache:
      - key: cache-$CI_COMMIT_REF_SLUG
        fallback_keys:
          - cache-$CI_DEFAULT_BRANCH
          - cache-default
        paths:
          - vendor/ruby
          - Gemfile.lock

    services:
      - docker:dind

    variables:
      CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_COMMIT_SHA
      DOCKER_DRIVER: overlay2
      ROLLOUT_RESOURCE_TYPE: deployment
      DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501
      RUNNER_GENERATE_ARTIFACTS_METADATA: "true"
      

    stages:
      - build
      - test
      - security
    
    include:
      - template: Security/SAST.gitlab-ci.yml

    build:
      stage: build
      variables:
        IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
      before_script:
        - docker info
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
      script:
        - docker build -t $IMAGE .
      after_script:
        - docker push $IMAGE
      artifacts:
        paths:
          - Gemfile.lock
        expire_in: 1 hour

    test:
      stage: test
      image: gliderlabs/herokuish:latest
      script:
        - cp -R . /tmp/app
        - /bin/herokuish buildpack test
      after_script:
        - echo "Our race track has been tested!"
      needs: []


    super_fast_test:
      stage: test
      script:
        - echo "If your not first you're last"
        - return 0
      needs: []
      rules:
        - if: $CI_COMMIT_BRANCH == 'main'
          allow_failure: true
   ```
2. Lets start by adding a new stage for our child pipeline. Edit the stage section to be the following:

   ```plaintext
   stages:
       - build
       - test
       - security
       - extra-security
   ```
3. Lets also add a job that calls the child pipeline in the security stage:

   ```plaintext
   downstream_security:
     stage: extra-security
     trigger:
       include:
         - local: security-pipeline/security.gitlab-ci.yml
   ```
4. Lets go ahead and click **Commit Changes** and use the left hand menu to click through **Build \> Pipelines**, then click the hyperlink from the most recently kicked off pipeline that starts with **<span dir="">_#_</span>**. In the pipeline view as the jobs run click into each of them to see how our added **_child-security-pipeline_** have changed the output.

> If you run into any issues you can use the left navigation menu to click through **Build \> Pipelines**, click **Run pipeline**, select **_child-security-pipeline_** and click **Run pipeline** once again.
