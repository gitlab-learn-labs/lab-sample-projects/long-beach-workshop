# Step 01 - Creating a Simple Pipeline

1. First click **_Long Beach Workshop_** in the top left of the screen. Now that we have our project imported, go ahead and open **_.gitlab-ci.yml_** located in Code -> Repository.
2. Notice that we have a simple pipeline already defined. We have two stages, **_build_** & **_test_**. Also notice a **_build_** job that uses the **_before_script , script, & after_script_** keywords.

3. In the **_test_** job we want to use the **_after_script_** keyword to echo out that the test has completed. First to edit the pipeline we need to click **Edit > Edit in pipeline editor**. Once there lets go ahead and add an **_after_script_** to the **_test_** job:

   ```plaintext
   after_script:
      - echo "Our race track has been tested!"
   ```

   Your new **_test_** job should look like this:

   ```plaintext
   test:
    stage: test
    image: gliderlabs/herokuish:latest
    script:
      - cp -R . /tmp/app
      - /bin/herokuish buildpack test
    after_script:
      - echo "Our race track has been tested!"
   ```

# Step 02 - Execution Order

1. In its current state, our pipeline will run jobs sequentially, but what if we wanted 2 jobs to run parallel? We can do that with the **_needs_** keyword. Lets navigate back to our **_.gitlab-ci.yml_** file to accomplish this.
2. Right now we only have the **_test_** job running during the test stage, so let's add the **_super_fast_test_** job below the **_test_** job.:

   ```plaintext
    super_fast_test:
      stage: test
      script:
        - echo "If youre not first youre last"
        - return 0
      needs: []
   ```
3. Now that we have the two jobs we also want to modify the execution order so that they run at the same time. Add the following line to the end of the **_test_** code block:

   ```plaintext
   needs: []
   ```

   The new **_test_** should look like this:

   ```plaintext
    test:
      stage: test
      image: gliderlabs/herokuish:latest
      script:
        - cp -R . /tmp/app
        - /bin/herokuish buildpack test
      after_script:
        - echo "Our race track has been tested!"
      needs: []
   ```

4. Go ahead and click **Commit changes**, then use the left hand navigation menu to click through **CI/CD -\> Pipelines** and click the hyperlink that starts with **_"#"_** on the most recently kicked off pipeline. As you watch you can see that the two jobs run in parallel.

> If you run into any issues you can use the left hand navigation menu to click through **CI/CD -\> Pipelines**, click **Run pipeline**, select **_execution-order_** and click **Run pipeline** once again.
