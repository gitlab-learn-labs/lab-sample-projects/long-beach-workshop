# Step 1: Parsing the Results
  1. Now that your **_main_** pipeline has completed the reports under **_Security & Compliance_** have been generated. These reports will only be generated when you run a pipeline for the default branch, in this case, **_main._**
  2. Use the left navigation menu to click  **Secure-\> Security Dashboard**. This will bring up a dashboard view of all of the security vulnerabilities & their counts over time so you can track your work as you secure your project. This dashboard takes time to collect data so if your dashboard does not show results, your presenter can show you the dashboard of a deployed Tanuki Racing application [here](https://gitlab.com/gitlab-learn-labs/webinars/tanuki-racing/tanuki-racing-application/-/security/dashboard) 

  3. Let's view the vulnerabilities in the **_Vulnerability Report._** Use the left navigation menu and click **Secure -\> Vulnerability Report** to view the full report
  4. First change the _All tools_ section under **Tools** to just filter on SAST. Click on any of the SAST vulnerabilities shown.
  5 Inside the vulnerability we will want to click the _Explain vulnerability_ button within the **Explain this vulnerability and how to mitigate it with AI** section. This will result in a popup appearing on the right hand side with some information on what the vulnerability is and how you can fix it. **_Explain vulnerability_** feature currently works on any SAST vulnerabilities.
# Step 2: Preventive Security Policies
  1. Now let's change the filter for **Severity** to **_critical_** & change **Tool** to **_Secret Detection._** We can click into any of the vulnerabilities present. We can see that one of our AWS tokens has already been leaked into the code base.

  2. To prevent this from happening in the future we can set up a new **_policy_** to run on future merge requests. For our use case, leaked tokens are easy mistakes that can lead to massive problems, so we will create a quick policy to stop that. Use the left navigation menu to click through **Secure \> Policies** and then click **New policy**. On the resulting page click **Select policy** under **_Scan result policy_**.
  3. Add a name to the policy, then under the **_Rules_** section we want to select **Security Scan** in the **When** dropdown list. Then we want to change **All scanners** to be **_Secret Detection_** and **All protected branches** to **default branch**.
  4. Then under **Actions** choose **individual users** as the **_Choose approver type_** and add **_lfstucker_** as the required approver and click **Configure with a merge request**. On the resulting merge request click ***merge*** and you will be brought to your new policy project that is applied to our workshop application. If you were to create another merge request with the leaked token still in the code based merging would be prevented until it was removed or you added your approval.
  5. Before we move on, lets go back to our project. Use the breadcrumbs at the top of the screen to click into your group, then once again click into your project.

> [Policy Documentation](https://docs.gitlab.com/ee/user/application_security/policies/)

# Step 3: Take Action on Our Vulnerabilities
  1. Now that we have a protective policy in place, let's go ahead and ensure it works by removing the Secrets currently in the code base. From the main page our project lets go ahead and click **Web IDE** in the **Edit** dropdown list.
  2. Click into the **_cf-sample-scripts/eks.yaml_** file and add our fake token **_aws_key_id AKIAIOSF0DNN7EXAMPLE_** at the end of the line 6. Change the **description** from **_The name of the IAM role for the EKS service to assume._** to **The name of the IAM role for the EKS service to assume, using aws_key_id AKIAIOSF0DNN7EXAMPLE.**
  3. Once added click the source control button on the left hand side, add a quick commit message, then click **Commit & Push**
  4. On the resulting drop down, next to **main**, click **Enter** to open a new branch, then click the **_Enter_** key. A new popup will appear where we want to then click **Create MR**
  5. Scroll to the bottom, uncheck **_Delete source branch when merge request is accepted_**, and click **Create merge request**
  6. On the resulting MR notice that our policy requires approval from **_lfstucker_** before we are able to merge. In order for us to merge in the future we will have to remove the token and wait for the full pipeline to run.

> [Documentation regarding automatically revoking secrets](https://docs.gitlab.com/ee/user/application_security/secret_detection/#responding-to-a-leaked-secret)

# Step 4: Review & Download Software Bill of Materials Report
  1. Using the left navigation menu click through **Secure \> Dependency list** to view all of the dependencies that are directly and indirectly included in your application.
  2. Click through a few of the pages and notice the components that are all directly/indirectly included in your application.
  3. Next click **Export** to download the SBOM report in CycloneDX json format. If you then open the download you can see all of the information displayed. To learn more about CycloneDX format go [here](https://cyclonedx.org/)

> [See how GitLab can be used to detect log4j](https://about.gitlab.com/blog/2021/12/15/use-gitlab-to-detect-vulnerabilities/)

# Step 5: License Compliance
  1. Using the left navigation menu click through the **Secure \> License Compliance** to view all of the licenses detected in your project.

  2. Let's say we decided we no longer want to allow the use of the MIT License. Using the left hand navigation menu click through the **Secure \> Policies** then click **New policy**.
  3. Click **Select policy** under **Scan result policy**
  4. In the **New scan result policy form** that appears, 
  * Provide the following mandatory information:
    * Name: **Deny MIT License**
    * Policy status: **Enabled**
    * Rules: If **Select scan type** = **License Scan**, rest of first section stays as is.
    * Set **Status is** to both **Newly Detected** and **Pre-existing**
    * Set License is **matching** **_MIT_**
    * Within Actions: Set Require 1 approval from: **Individual users** **lfstucker**
    * Click **Configure with a merge request**
  5. Click **Merge** to merge the new merge request into the existing security policy project.
  6. Go back to your project using the breadcrumb, clicking on your group, then clicking on your project.
  7. If we were to run a new pipeline for a MR, a new approval rule based on this license compliance policy will be added to prevent any software using the MIT license from being merged and the security bot will notify you that you have a policy violation
  8. Lastly if you use the left hand navigation menu you can click through **Secure > License Compliance** and see that we have been notified that the MIT license is a Policy violation.
